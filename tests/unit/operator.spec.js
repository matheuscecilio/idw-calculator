import { add, sub, div, mult } from '@/classes/Operators'

describe('Operadores Aritmeticos', () => {
  it('Soma Ok', () => {
    const valueA = 4
    const valueB = 7
    const result = 11
    expect(add(valueA, valueB)).toBe(result)
  })
  it('Subtração Ok', () => {
    const valueA = 4
    const valueB = 7
    const result = -3
    expect(sub(valueA, valueB)).toBe(result)
  })
  it('Multiplicação Ok', () => {
    const valueA = 7
    const valueB = 7
    const result = 49
    expect(mult(valueA, valueB)).toBe(result)
  })
  it('Divisão Ok', () => {
    const valueA = 100
    const valueB = 5
    const result = 20
    expect(div(valueA, valueB)).toBe(result)
  })
})
